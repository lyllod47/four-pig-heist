﻿using UnityEngine;
using System.Collections; 


public class EnvironmentPool : MonoBehaviour {

	public EnvironmentLanes [] enviPoolList; 
	public float PoolOffset; 

	void OnEnable(){
		GameEvent.OnEnvironmentTriggeredE += GameEvent_OnEnvironmentTriggeredE;
	}

	void OnDisable(){
		GameEvent.OnEnvironmentTriggeredE -= GameEvent_OnEnvironmentTriggeredE;
	}

	void GameEvent_OnEnvironmentTriggeredE (int enviID){ 
		enviPoolList [enviID - 1].RegenerateLanes ();
		enviPoolList [enviID - 1].RefreshEnvi ();
		enviPoolList [enviID - 1].transform.position = enviPoolList [enviID - 1].transform.position + new Vector3 (0,0,PoolOffset);
 

	}
 




}
