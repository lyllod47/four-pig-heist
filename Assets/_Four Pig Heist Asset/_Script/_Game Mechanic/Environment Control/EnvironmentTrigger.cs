﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class EnvironmentTrigger : MonoBehaviour {


	public int enviID;



	void OnTriggerEnter(Collider col){
		if(col.tag == GameParam.TAG_PLAYER){ 
			if(col.gameObject.GetComponent<PigPlayerController>().enabled ){
				WaitAndPool();
			}
		}
	}

	Sequence waitPool;

	void WaitAndPool(){
		if(waitPool!=null && waitPool.IsActive() ){
			waitPool.Kill ();
		}

		waitPool = DOTween.Sequence ();
		waitPool.InsertCallback (1f , ()=> GameEvent.OnEnvironmentTriggered(enviID) );
	}

}
