﻿using UnityEngine;
using System.Collections;

public class EnvironmentSides : MonoBehaviour {

	public Transform[] SideStones; 

	public void SortStones(){
		for (int i = 0; i < SideStones.Length; i++) {
			float randomY = Random.Range (3f , 15f);
			SideStones [i].transform.localScale = new Vector3 (2,randomY,2);
		}
	}

}
