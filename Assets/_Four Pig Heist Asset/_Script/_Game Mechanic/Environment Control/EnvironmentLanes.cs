﻿using UnityEngine;
using System.Collections;

public class EnvironmentLanes : MonoBehaviour {


	public Transform[] myLanes; 
 
	public HitObjectPool coinPool , stonePool , cratesPool;
	public EnvironmentSides enviRight , enviLeft;

	void Start(){
		RefreshEnvi ();
	}

	void OnEnable(){
		GameEvent.OnGameStartE += GameEvent_OnGameStartE;
	}

	void OnDisable(){
		GameEvent.OnGameStartE -= GameEvent_OnGameStartE;
	}

	void GameEvent_OnGameStartE (){
		RegenerateLanes ();
	}


	public void RefreshEnvi(){
		enviRight.SortStones ();
		enviLeft.SortStones ();
	}
 
	public void RegenerateLanes(){
		for (int i = 0; i < myLanes.Length; i++) {
			GenerateCoins (myLanes[i]);
		}

		for (int i = 0; i < myLanes.Length; i++) {
			GenerateStones (myLanes[i]);
		}

		for (int i = 0; i < myLanes.Length; i++) {
			GenerateCrates (myLanes[i]);
		}
 
	}


	 void GenerateCoins(Transform parent){
		float randomZ = Random.Range (-5f , 5f);
		float randomY = Random.Range (1f , 3f);

		GameObject newCoin = coinPool.GetItem ();
		newCoin.transform.SetParent (parent);
		newCoin.SetActive (true);
		newCoin.transform.localPosition = new Vector3 ( 0 , randomY , randomZ);
	
	}


	void GenerateStones(Transform parent){
		float randomZ = Random.Range (-5f , 5f);
		GameObject newStone = stonePool.GetItem ();
		newStone.transform.SetParent (parent);
		newStone.SetActive (true);
		newStone.transform.localPosition = new Vector3 ( 0 , 0 , randomZ);
	}

	void GenerateCrates (Transform parent){
		float randomZ = Random.Range (-5f , 5f);
		GameObject newCrate = cratesPool.GetItem ();
		newCrate.transform.SetParent (parent);
		newCrate.SetActive (true);
		newCrate.transform.localPosition = new Vector3 ( 0 , 0 , randomZ);
	}


}
