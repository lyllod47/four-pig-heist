﻿using UnityEngine;
using System.Collections;

public class HitObjectTrigger : MonoBehaviour {
 


	public OBJECT_IN_GAME objectType;

	void OnTriggerEnter(Collider col){
		if(col.tag == GameParam.TAG_PLAYER){ 


			switch (objectType) {

			case OBJECT_IN_GAME.COINS:
				col.gameObject.GetComponent<PigMotor> ().OnGetCoin ();
				break;

			case OBJECT_IN_GAME.OBSTACLE:
				col.gameObject.GetComponent<PigMotor> ().OnHitObstacle ();
				break;

			case OBJECT_IN_GAME.POWERUP_BOX:
				col.gameObject.GetComponent<PigMotor> ().GeneratePowerUp ();
				break; 
			}
			gameObject.SetActive (false);
		}
	}







}
