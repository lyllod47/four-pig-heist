﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using UnityEngine.UI;
using DG.Tweening;

public class GameController : MonoBehaviour {


	public static GameController Instance;
	public List<PigPlayer> playerList;

	public Text txtCountDown;
	Sequence countDownSequence;

	public int currentPlayerID;

	void Awake(){
		Instance = this;
	}

	void Start(){
		_OnStartGame ();
	}
 
	public void _OnStartGame(){
		SetRandomPlayer ();
		SetCountDown ();
	}

	void SetRandomPlayer(){

		int randomPlayer = Random.Range (0,playerList.Count);
		currentPlayerID = randomPlayer;

		for (int i = 0; i < playerList.Count; i++) {
			playerList [i].SetStartingPost (i);
			playerList [i].SetID (i);

			if (i == randomPlayer) {
				playerList [i].SetThisAsPlayer ();
			} else {
				playerList [i].SetThisAsBot ();
			}
		}
	}



	void SetCountDown(){
		if(countDownSequence!=null && countDownSequence.IsActive() ){
			countDownSequence.Kill ();
		}

		countDownSequence = DOTween.Sequence ();	 
		txtCountDown.gameObject.SetActive (true);


		countDownSequence.AppendCallback ( ()=> ResetText ("3"));
		countDownSequence.AppendInterval (0.1f);
		countDownSequence.Append (  txtCountDown.rectTransform.DOScale(0f , 1f ).SetEase(Ease.InCirc));


		countDownSequence.AppendCallback ( ()=> ResetText ("2"));
		countDownSequence.AppendInterval (0.1f);
		countDownSequence.Append (  txtCountDown.rectTransform.DOScale(0f , 1f ).SetEase(Ease.InCirc));
 

		countDownSequence.AppendCallback ( ()=> ResetText ("1"));
		countDownSequence.AppendInterval (0.1f);
		countDownSequence.Append (  txtCountDown.rectTransform.DOScale(0f , 1f ).SetEase(Ease.InCirc)); 

		countDownSequence.AppendCallback (ReleaseThePigs);
	}

	void ResetText(string msg){
		txtCountDown.rectTransform.localScale = Vector3.one;
		txtCountDown.text = msg;
	}

	void ReleaseThePigs(){
		txtCountDown.gameObject.SetActive (false);
		GameEvent.OnGameStart ();
		for (int i = 0; i < playerList.Count; i++) {
			playerList [i].ReleasePig ();
		}
	}



//	public void AddPlayer(PigPlayerController player){
//		player.SetStartingPost ( playerList.Count );
//		playerList.Add (player);
//	}


//	public override void OnNetworkDestroy(){
//		playerList = new List<PigController> ();
//	}


}
