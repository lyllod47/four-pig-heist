﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class CandleTimer : MonoBehaviour
{ 
	public float currentTime , originTargetTime; 
	public Text txtTimer;
    public RectTransform candleTransform;
	public GameObject candleFire;

	float originCandleHeight = 100;



	void OnEnable(){
		GameEvent.OnGameStartE += StartCandle;
		GameEvent.OnPigAttackedE += GameEvent_OnPigAttackedE;
	} 

	void OnDisable(){
		GameEvent.OnGameStartE -= StartCandle;
		GameEvent.OnPigAttackedE -= GameEvent_OnPigAttackedE;
	}


	void GameEvent_OnPigAttackedE (){ 
		currentTime -= 1f;
		UpdateCandleSize ();
	}

    void StartCandle(){
		originTargetTime = Random.Range (60,120);
//		originTargetTime = 1f;


		currentTime = originTargetTime;
		candleFire.SetActive (true);
		candleTransform.sizeDelta = new Vector2 (25,originCandleHeight); 
		StartCoroutine( MeltCandle()  );
    }

	void OnCandleOff(){
		candleFire.SetActive (false);
		GameEvent.OnGameOver ();
	}

	IEnumerator MeltCandle(){

		while (currentTime >= 0) {
			currentTime -= 0.1f ;
			UpdateCandleSize ();
			yield return new WaitForSeconds (0.1f);
		}
		OnCandleOff ();
	}

	void UpdateCandleSize(){
		float newHeight = (currentTime / originTargetTime) * originCandleHeight;
		candleTransform.sizeDelta = new Vector2 (25,newHeight);

		txtTimer.text = (int)currentTime + "";
	}




 
}
