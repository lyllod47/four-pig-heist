﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Com.LuisPedroFonseca.ProCamera2D;

public class PigMotor : MonoBehaviour {

	public int pigID;
	LANES currentLanes , targetLanes;	
	public bool isCanMove;
	public Transform pigMesh;
	public float moveSpeed;
	public float dashMultiplier;
	
	//	public float slideMultiplier;
	
	float sideRayDistance = 2f , forwardRayDistance = 3f;
	float originSpeed ;
	PigBotController mBotController;

	Transform myTransform;
	Tween changeLaneTween;
	Sequence jumpingSeq; 
	Sequence jumpBackSeq;
	Sequence dashSeq;
	Sequence slideSeq;
	
	 PigMotor enemyFront , enemyLeft , enemyRight;
	RaycastHit hit_front , hit_left , hit_right; 



	void Start(){
		myTransform = transform;
		originSpeed = moveSpeed;
		mBotController = GetComponent<PigBotController> ();
	}	

	public void SetCurrentLane(LANES lane){
		currentLanes = lane;
	}
 
	void FixedUpdate(){
		CheckNearbyPlayer ();
		AutoMove ();	
	} 

	void AutoMove(){
		if(isCanMove){
			myTransform.Translate (Vector3.forward * moveSpeed * Time.deltaTime);
		}
	}
 
	void CheckNearbyPlayer(){
		
		if (enemyFront != null) {
			Debug.DrawLine (pigMesh.position, pigMesh.position + (Vector3.forward * forwardRayDistance) , Color.red);
		} else {
			Debug.DrawLine (pigMesh.position, pigMesh.position + (Vector3.forward * forwardRayDistance) , Color.green);
		}
		
		
		if (enemyLeft != null) {
			Debug.DrawLine (pigMesh.position , pigMesh.position + (Vector3.left*sideRayDistance) , Color.red );
		} else {
			Debug.DrawLine (pigMesh.position , pigMesh.position + (Vector3.left*sideRayDistance) , Color.green );
		}
		
		if (enemyRight != null) {
			Debug.DrawLine (pigMesh.position , pigMesh.position + (Vector3.right*sideRayDistance) , Color.red );
		} else {
			Debug.DrawLine (pigMesh.position , pigMesh.position + (Vector3.right*sideRayDistance) , Color.green );
		}
		
		
		if (Physics.Raycast (pigMesh.position, (Vector3.forward), out hit_front, forwardRayDistance)) {
			if(hit_front.collider.tag == GameParam.TAG_PLAYER){
				enemyFront = hit_front.collider.gameObject.GetComponent<PigMotor> ();
			}
		}  else {
			enemyFront = null;
		}
		
		if (Physics.Raycast (pigMesh.position, (Vector3.left), out hit_left, sideRayDistance)) {
			if(hit_left.collider.tag == GameParam.TAG_PLAYER){
				enemyLeft = hit_left.collider.gameObject.GetComponent<PigMotor> ();
			}
		}   else {
			enemyLeft = null;
		}
		
		if (Physics.Raycast (pigMesh.position, (Vector3.right*sideRayDistance), out hit_right, sideRayDistance)) {
			if(hit_right.collider.tag == GameParam.TAG_PLAYER){
				enemyRight = hit_right.collider.gameObject.GetComponent<PigMotor> ();
			} 
		} else {
			enemyRight = null;
		}
		
	}
	 
	
	void Kill_LaneTween(){
		if(changeLaneTween!=null && changeLaneTween.IsActive() ){
			changeLaneTween.Kill ();
		}
	}
	
	public void MoveLeft(){
		if(!isCanMove){
			return;
		}

		if(currentLanes == LANES.OUTER_LEFT){
			return;
		}
		
		Kill_LaneTween ();
		
		switch (currentLanes) {
		
		case LANES.OUTER_LEFT:
			break;
			
		case LANES.LEFT:
			targetLanes = LANES.OUTER_LEFT;
			break;
			
		case LANES.RIGHT:
			targetLanes = LANES.LEFT;
			break;
			
		case LANES.OUTER_RIGHT:
			targetLanes = LANES.RIGHT;
			break;
			
		}
		
		float targetX = GameParam.LanesXPost[(int)targetLanes];
		changeLaneTween = myTransform.DOMoveX (targetX , 0.3f);
		currentLanes = targetLanes;
		if(enemyLeft!=null){
			if (!mBotController.enabled) {
				AudioController.Play ("PigAttack");
			}
			enemyLeft.OnAttacked ();
		}
	}
	
	
	
	public void MoveRight(){
		if(!isCanMove){
			return;
		}

		if(currentLanes == LANES.OUTER_RIGHT){
			return;
		}
		
		Kill_LaneTween ();
		
		switch (currentLanes) {
		
		case LANES.OUTER_LEFT:
			targetLanes = LANES.LEFT;
			break;
			
		case LANES.LEFT:
			targetLanes = LANES.RIGHT;
			break;
			
		case LANES.RIGHT:
			targetLanes = LANES.OUTER_RIGHT;
			break;
			
		case LANES.OUTER_RIGHT: 
			break;
			
		}
		
		float targetX = GameParam.LanesXPost[(int)targetLanes];
		changeLaneTween = myTransform.DOMoveX (targetX , 0.3f);
		currentLanes = targetLanes;
		
		if(enemyRight!=null){
			if (!mBotController.enabled) {
				AudioController.Play ("PigAttack");
			}
			enemyRight.OnAttacked ();
		}
	}
	
	
	public void JumpUp(){		

		if(!isCanMove){
			return;
		}

		Jump ();
		pigMesh.DOLocalRotate ( Vector3.right*360f  , 0.5f , RotateMode.FastBeyond360)  ;
	} 
	
	public void JumpBack(){ 
		if(!isCanMove){
			return;
		}

		if(jumpBackSeq!=null && jumpBackSeq.IsActive() ){
			return;
		}

		jumpBackSeq = DOTween.Sequence ();
		moveSpeed = 0f;

		jumpBackSeq = DOTween.Sequence ();
		pigMesh.DOLocalRotate ( Vector3.left*360f  , 0.5f , RotateMode.FastBeyond360)  ;
		jumpBackSeq.Append ( myTransform.DOScaleY (0.8f  , 0.1f) );
		jumpBackSeq.Append ( myTransform.DOMoveY (2 ,0.3f).SetEase(Ease.OutBack)); 
		jumpBackSeq.AppendCallback (FallDownAnim );  
		jumpBackSeq.InsertCallback (0.5f , ()=> moveSpeed = originSpeed);
	}
	
	void Jump(){
		if(!isCanMove){
			return;
		}


		if(jumpingSeq!=null && jumpingSeq.IsActive() ){
			return;
		}
		jumpingSeq = DOTween.Sequence ();
		
		jumpingSeq.Append ( myTransform.DOScaleY (0.8f  , 0.1f) );
		jumpingSeq.Append ( myTransform.DOMoveY (2 ,0.3f).SetEase(Ease.OutBack)); 
		jumpingSeq.AppendCallback (FallDownAnim );  
	}
	
	void FallDownAnim(){
		myTransform.DOMoveY (0, 0.3f).SetEase (Ease.InCirc);
		myTransform.DOScaleY (1f, 0.2f);
	}
	
 
	
	public void Dash(){
		if(!isCanMove){
			return;
		}


		if(dashSeq!=null && dashSeq.IsActive() ){
			return;
		}

		AudioController.Play ("PigDash");
		moveSpeed *= dashMultiplier; 
		dashSeq = DOTween.Sequence ();
		dashSeq.Insert (0f , pigMesh.DOLocalRotate ( Vector3.back*720f  , 0.5f , RotateMode.FastBeyond360) );
		dashSeq.InsertCallback (1f , ()=> moveSpeed = originSpeed);
		Attack ();
		
		
	}
	
	public void Attack(){
		//		Debug.Log ("On Attack");
		if(!mBotController.enabled){
			ProCamera2DShake.Instance.ShakeUsingPreset("SenggolBacok");
		}

	

		if(enemyFront!=null){

			if (!mBotController.enabled) {
				AudioController.Play ("PigAttack");
			}

			enemyFront.OnAttacked ();
		}
	}
  
	public void OnAttacked(){ 
		
		if (!mBotController.enabled) {
			AudioController.Play ("PigAttack");
		}

		JumpBack ();
		GameEvent.OnPigAttacked ();
		if(mBotController.enabled){
			mBotController.OnPigAttacked ();
		}
	} 

	public void OnHitObstacle(){
		if (!mBotController.enabled) {
			ProCamera2DShake.Instance.ShakeUsingPreset("ObstacleHit");
		}

		AudioController.Play ("HitObstacle"); 
		JumpBack ();
	}
	 
	public void GeneratePowerUp(){
		Debug.Log ("GeneratePowerUp");
	}

	public void UsePowerUp(){
		Debug.Log ("UsePowerUp");
	}

	public void OnGetCoin(){
		if (!mBotController.enabled) {
			AudioController.Play ("CoinSound");
		}
		GameEvent.OnGetCoin (pigID); 
	}
	
}
