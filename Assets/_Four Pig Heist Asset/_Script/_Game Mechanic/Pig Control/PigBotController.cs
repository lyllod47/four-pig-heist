﻿using UnityEngine;
using System.Collections;

 
public class PigBotController : MonoBehaviour {

	PigMotor pigMotor;
	bool isCanThink;

	void OnEnable(){
		GameEvent.OnGameStartE += StartPig;
		GameEvent.OnGameOverE += StopPig;
	}

	void OnDisable(){
		GameEvent.OnGameStartE -= StartPig;
		GameEvent.OnGameOverE -= StopPig;
	}

	void Start(){
		if(pigMotor == null){
			pigMotor = GetComponent<PigMotor> ();
		}
		isCanThink = false;
	}

	void StartPig(){		
//		InvokeRepeating ("DecideAction" , 1f , Random.Range(0.3f , 2f) );
		isCanThink = true;
		StartCoroutine ( PigBehaviour() );
	}

	IEnumerator PigBehaviour(){
		while (isCanThink) {
			DecideAction ();
			yield return new WaitForSeconds ( Random.Range(0.3f , 2f) );
		}

	}

	public void StopPig(){ 
		isCanThink = false;
//		CancelInvoke ();
	} 

	public void OnPigAttacked(){
		if(Random.Range(0,10) > 7 ){
			pigMotor.Dash ();
		}
	}

	void DecideAction(){


		int randomDash = Random.Range (0,10);

		if(randomDash < 5){
			pigMotor.Dash ();
			return;
		}

		int randomAction = Random.Range (0, 6);
		switch ( (PIG_ACTIONS) randomAction ) {
		case PIG_ACTIONS.NONE:
			break;

		case PIG_ACTIONS.JUMP:
			pigMotor.JumpUp ();
			break;

		case PIG_ACTIONS.MOVE_LEFT:
			pigMotor.MoveLeft ();
			break;

		case PIG_ACTIONS.MOVE_RIGHT:
			pigMotor.MoveRight ();
			break;

		case PIG_ACTIONS.JUMP_BACK:
			pigMotor.JumpBack ();
			break;

		case PIG_ACTIONS.DASH:
			pigMotor.Dash ();
			break;

		case PIG_ACTIONS.USE_POWER_UPS:
			pigMotor.UsePowerUp ();
			break;

		default:
			pigMotor.Dash ();
			break;

		}

	}




}
