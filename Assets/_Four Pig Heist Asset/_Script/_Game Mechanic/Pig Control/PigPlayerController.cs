﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(PigMotor))]
public class PigPlayerController : MonoBehaviour {
	
	PigMotor pigMotor; 
	
	void Start(){
		pigMotor = GetComponent<PigMotor> ();
	} 	 
 
	void OnEnable(){
		GameEvent.OnPlayerInput_JumpE += Handle_OnJump;
		GameEvent.OnPlayerInput_MoveLeftE += Handle_OnMoveLeft;
		GameEvent.OnPlayerInput_MoveRightE += Handle_OnMoveRight;
		GameEvent.OnPlayerInput_JumpBackE += Handle_OnJumpBack;
		GameEvent.OnPlayerInput_DashE += Handle_OnDash;
		GameEvent.OnPlayerInput_PowerUpE += Handle_OnPowerUp;
	}

	void OnDisable(){
		GameEvent.OnPlayerInput_JumpE -= Handle_OnJump;
		GameEvent.OnPlayerInput_MoveLeftE -= Handle_OnMoveLeft;
		GameEvent.OnPlayerInput_MoveRightE -= Handle_OnMoveRight;
		GameEvent.OnPlayerInput_JumpBackE -= Handle_OnJumpBack;
		GameEvent.OnPlayerInput_DashE -= Handle_OnDash;
		GameEvent.OnPlayerInput_PowerUpE -= Handle_OnPowerUp;
	}

	void Handle_OnJump (){  pigMotor.JumpUp ();  }
	void Handle_OnMoveLeft (){  pigMotor.MoveLeft ();  }
	void Handle_OnMoveRight (){  pigMotor.MoveRight ();  }
	void Handle_OnJumpBack (){  pigMotor.JumpBack ();  }
	void Handle_OnDash (){  pigMotor.Dash ();  }
	void Handle_OnPowerUp (){  pigMotor.UsePowerUp ();  } 

 
	void Update(){
		DebugButtons ();		
	} 
	
	void DebugButtons(){
		if(Input.GetKeyDown(KeyCode.A)){
			GameEvent.OnPlayerInput_MoveLeft ();
		}
		
		if(Input.GetKeyDown(KeyCode.D)){
			GameEvent.OnPlayerInput_MoveRight ();
		}
		
		if(Input.GetKeyDown(KeyCode.W)){
			GameEvent.OnPlayerInput_Jump ();
		}
		
		if(Input.GetKeyDown(KeyCode.S)){
			GameEvent.OnPlayerInput_JumpBack ();
		}
		
		if(Input.GetKeyDown(KeyCode.LeftShift)){
			GameEvent.OnPlayerInput_Dash ();
		}
 
		if(Input.GetKeyDown(KeyCode.Space)){
			GameEvent.OnPlayerInput_PowerUp ();
		}
		
		
	} 
	
}//EoF
