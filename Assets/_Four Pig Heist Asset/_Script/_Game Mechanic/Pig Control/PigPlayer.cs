﻿using UnityEngine;
using System.Collections;
using Com.LuisPedroFonseca.ProCamera2D;

public class PigPlayer : MonoBehaviour {

 
	public PigPlayerController mController;
	public PigBotController mBotController;
	public PigMotor pigMotor;


	void OnEnable(){
		GameEvent.OnGameOverE += StopPig;
	}

	void OnDisable(){
		GameEvent.OnGameOverE -= StopPig;
	}


	public void StopPig(){
		pigMotor.isCanMove = false;
		pigMotor.JumpBack ();

	}


	public void SetID(int id){
		pigMotor.pigID = id;
	}

	public void SetThisAsPlayer(){
//		Debug.Log ("SetThisAsPlayer "+name);
		mController.enabled = true;			 
		Camera.main.GetComponent<ProCamera2D> ().AddCameraTarget (transform);
		Camera.main.GetComponent<ProCamera2D> ().GetCameraTarget (transform).TargetOffset = new Vector2 (0, -6f);
		//		GameController.Instance.AddPlayer ( GetComponent<PigController> () );

		mController.enabled = true;
		mBotController.enabled = false;
	}	 

	public void SetThisAsBot(){
//		Debug.Log ("SetThisAsBot "+name);
		mController.enabled = false;
		mBotController.enabled = true;
	}

	public void SetStartingPost(int player_id){
		transform.position = new Vector3 ( GameParam.LanesXPost[player_id] , 0 , 5);
		pigMotor.SetCurrentLane( (LANES)player_id );
	}

	public void ReleasePig(){
//		Debug.Log ("ReleasePig "+name);
		pigMotor.isCanMove = true;
	}



}
