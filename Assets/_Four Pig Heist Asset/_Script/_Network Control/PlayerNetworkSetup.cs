﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using Com.LuisPedroFonseca.ProCamera2D;

public class PlayerNetworkSetup : NetworkBehaviour {


	void Start(){
		if (isLocalPlayer) {
			GetComponent<PigPlayerController> ().enabled = true;
			Camera.main.GetComponent<ProCamera2D> ().AddCameraTarget (transform);
			Camera.main.GetComponent<ProCamera2D> ().GetCameraTarget (transform).TargetOffset = new Vector2 (0, -6f);
//			GameController.Instance.AddPlayer ( GetComponent<PigPlayerController> () );
		}
	}



 
}
