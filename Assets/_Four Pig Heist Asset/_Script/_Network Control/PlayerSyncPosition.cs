﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerSyncPosition : NetworkBehaviour {

	[SyncVar]
	private Vector3 syncPost;

	[SerializeField] Transform myTransform;
	[SerializeField] float lerpRate = 25;

	private Vector3 lastPos;
	private float threshold = 0.5f;


	void FixedUpdate(){
		TransmitPosition ();
		LerpPosition ();
	}

	void LerpPosition(){
		if(!isLocalPlayer){
			myTransform.position = Vector3.Lerp (myTransform.position , syncPost , Time.deltaTime*lerpRate);
//			myTransform.position = syncPost;
		}
	}

	[Command]
	void CmdProvidePositionToServer(Vector3 pos){
		syncPost = pos;
	}

	[ClientCallback]
	void TransmitPosition(){
		if(isLocalPlayer && Vector3.Distance(myTransform.position , lastPos) > threshold ){
			CmdProvidePositionToServer (myTransform.position);
			lastPos = myTransform.position;
		}
	}

}
