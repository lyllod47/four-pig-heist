﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class TitleScreenButton : MonoBehaviour {

 
    public GameObject selectPigPanel; 
    public GameObject closeGameButton;
    public GameObject quitPanel;  

    public GameObject title;

	// Use this for initialization
	void Start () {
        selectPigPanel.SetActive(false);
        quitPanel.SetActive(false);

	}
	 

    public void StartGame()
    {
//		Application.LoadLevel(GameParam.SCENE_GAME);
		SceneManager.LoadScene (GameParam.SCENE_GAME);
    }

    public void CloseGamePanel()
    {
        quitPanel.SetActive(true);
    }

    public void CloseGamePanelYes()
    {
        Application.Quit();
    }

    public void CloseGamePanelNo()
    {
        quitPanel.SetActive(false);
    }

    public void SelectPigButton()
    {
        selectPigPanel.SetActive(true);
        title.SetActive(false);
        closeGameButton.SetActive(false);
    }

    public void SelectPigPanelCloseButton()
    {
        selectPigPanel.SetActive(false);
        title.SetActive(true);
        closeGameButton.SetActive(false);
    }

    public void SelectPig()
    {

    }
}
