﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_Controller_GameInput : MonoBehaviour , IPointerDownHandler , IPointerUpHandler {

 
    public GameObject pausePanel; 

    public Image powerUpButtonImage;
	public Sprite[] powerup_sprites;

 
	void OnEnable(){
		GameEvent.OnPickUpPowerUpE += CollectedPowerUp;
	}

	void OnDisable(){
		GameEvent.OnPickUpPowerUpE -= CollectedPowerUp;
	}

	void CollectedPowerUp(POWER_UP type){ 
		powerUpButtonImage.sprite = powerup_sprites[(int)type]; 
	}

	/*******************/


	#region SwipeHandler
	Vector2 startPost , endPost , deltaPost;
	public float SwipeTreshold;
	public void OnPointerDown(PointerEventData eventData){
		startPost = eventData.position; 
	}

	public void OnPointerUp(PointerEventData eventData){
		endPost = eventData.position; 
		deltaPost = endPost - startPost; 

		if (Mathf.Abs (deltaPost.x) > Mathf.Abs (deltaPost.y)) {

			if(Mathf.Abs(deltaPost.x) > SwipeTreshold){
				if(deltaPost.x < 0  ){
					GameEvent.OnPlayerInput_MoveLeft ();
				} else  {
					GameEvent.OnPlayerInput_MoveRight ();
				}
			}


		} else {
			if (Mathf.Abs (deltaPost.y) > SwipeTreshold) {
				if(deltaPost.y < 0 ){
					GameEvent.OnPlayerInput_JumpBack ();
				} else{
					GameEvent.OnPlayerInput_Jump ();
				}
			}
		}
	}

	#endregion


	#region Button Handler

    public void _OnPauseGame(){
        Time.timeScale = 0.01f;
        pausePanel.SetActive(true);
    }

    public void _OnResumeGame() {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    public void _OnDash(){
		GameEvent.OnPlayerInput_Dash (); 
    }

    public void _OnUseItem(){
		GameEvent.OnPlayerInput_PowerUp ();
    }

	#endregion

  
 
    
}
