﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Controller_ResultScreen : MonoBehaviour {
	
	Sequence popUpSeq; 		
	public GameObject resultPanel;
	public GameObject winText;
	public GameObject loseText; 
	public GameObject retryButton;
	public GameObject mainMenuButton;	
	
	public void ShowResultScreen(bool isPlayerWin){
		
		winText.SetActive(false);
		loseText.SetActive(false);      
		resultPanel.SetActive(true);
		
		if (popUpSeq != null && popUpSeq.IsActive()){
			return;
		}

		popUpSeq = DOTween.Sequence();
		popUpSeq.Append(resultPanel.transform.DOScale(0.8f, 0.5f).SetEase(Ease.OutBounce));
		
		if(isPlayerWin){
			winText.SetActive(true);
			popUpSeq.Append(winText.transform.DOScale(1.0f, 0.3f).SetEase(Ease.OutBounce));
		} else {
			loseText.SetActive(true);
			popUpSeq.Append(loseText.transform.DOScale(1.0f, 0.3f).SetEase(Ease.OutBounce));
		}		
	}
	
	public void _OnHomeButton(){ 
		SceneManager.LoadScene (GameParam.SCENE_TITLE);
	}
	
	public void _OnRetryButton() { 
		SceneManager.LoadScene (GameParam.SCENE_GAME);
	}
 
}