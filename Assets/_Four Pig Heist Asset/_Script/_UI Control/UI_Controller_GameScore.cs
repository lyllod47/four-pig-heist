﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Controller_GameScore : MonoBehaviour {
	
	public Text[] txtScores;
	public int[] scoreValues = new int[4];


	public Text txtTargetCoin;
	public int targetCoin;

	public UI_Controller_ResultScreen resultController;

	void OnEnable(){
		GameEvent.OnGameStartE += ResetScore;
		GameEvent.OnGameOverE += CalculateScore;
		GameEvent.OnGetCoinE += GameEvent_OnGetCoinE;
	}
	
	void OnDisable(){
		GameEvent.OnGameStartE -= ResetScore;
		GameEvent.OnGameOverE -= CalculateScore;
		GameEvent.OnGetCoinE -= GameEvent_OnGetCoinE;
	}
	
	void GameEvent_OnGetCoinE (int pigID){
		scoreValues [pigID]++;
		UpdateText ();
	}
	
	void ResetScore(){
		scoreValues = new int[4];
		targetCoin = Random.Range (10,20);
		UpdateText ();
	}

	void UpdateText(){
		for (int i = 0; i < txtScores.Length; i++) {
			txtScores [i].text = scoreValues [i].ToString();
		}
		txtTargetCoin.text = targetCoin.ToString();
	}




	int GetHighestIndex(){ 
		
		int maxIndex = 0;
		int maxScore = 0;

		for (int i = 0; i < scoreValues.Length; i++) {
			if(scoreValues[i] > maxScore){
				maxScore = scoreValues [i];
				maxIndex = i;
			}
		}

		return maxIndex;

	}


	void CalculateScore(){

		if (GetHighestIndex () == GameController.Instance.currentPlayerID && scoreValues[GetHighestIndex ()] >= targetCoin) {
			resultController.ShowResultScreen (true);
		} else {
			resultController.ShowResultScreen (false);
		}


	}


	
	
}
