﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class BG_Mover : MonoBehaviour {
	
	public Transform[] objList;
	public List<Tween> tweenList;
	public float animTime, moveOffset;
	Sequence waitCooldown;
	public bool isCanContinueMoving;

	void Start(){	
		isCanContinueMoving = true;
		Initialize ();	
	}		
 

	void OnDisable(){
		for (int i = 0; i < tweenList.Count; i++) {
			if(tweenList [i]!=null && tweenList [i].IsActive()  ){
				tweenList [i].Kill ();
			}
		}
	}


	
	void HoldBG_Punching(int comboID){
		PauseAll ();

		if(waitCooldown!= null && waitCooldown.IsActive() ){
			waitCooldown.Kill ();
		}
	
		if(isCanContinueMoving){
			waitCooldown = DOTween.Sequence ();
			waitCooldown.InsertCallback ( 0.4f , ResumeTween ) ;
		}
	}

	void HoldBG_Hit(){
		PauseAll ();

		if(waitCooldown!= null && waitCooldown.IsActive()){
			waitCooldown.Kill ();
		}
		if(isCanContinueMoving){
			waitCooldown = DOTween.Sequence ();
			waitCooldown.InsertCallback ( 0.2f , ResumeTween ) ;
		}
	}

	void PauseTween () 	{ 	
		isCanContinueMoving = false;
		PauseAll ();
	}

	void ResumeTween()	{	
		isCanContinueMoving = true;
		ResumeAll ();
	}
	
	void Initialize(){		
		tweenList = new List<Tween> ();
		for (int i = 0; i < objList.Length; i++) {
			tweenList.Add( MoveTargetNext ( objList [i] , i ).Play()  );
		}		 
	}	

	 

	Tween MoveTargetNext(Transform target , int mIndex){
		float targetX =target.localPosition.x - moveOffset;
		Tween result = target.DOLocalMoveX( targetX , animTime)
			.SetEase(Ease.Linear)
			.OnComplete( ()=> OnReuse (target , mIndex) ) ;
		
		try {
			tweenList [mIndex] = result;
		} catch (System.Exception ex) {
			Debug.Log (ex);
		}


		return result;
	}
	
	void OnReuse(Transform target , int mIndex){		
		if(target.localPosition.x < 0 ){
			float targetX = (objList.Length - 1) * moveOffset;
			target.localPosition = new Vector3 (targetX,0,0);
		}

		MoveTargetNext (target , mIndex).Play();
	}


	void PauseAll(){
		for (int i = 0; i < tweenList.Count; i++) {
			if(tweenList [i]!=null && tweenList [i].IsActive()  ){
				tweenList [i].Pause ();
			}
		}
	}

	void ResumeAll(){
		for (int i = 0; i < tweenList.Count; i++) {
			if(tweenList [i]!=null && tweenList [i].IsActive()  ){
				tweenList [i].Pause ();
				tweenList [i].TogglePause ();
			}
		}
	}


}