﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(AutoSorter))]
[CanEditMultipleObjects]


public class AutoSorterEditor : Editor {

	AutoSorter parent;
	
	void OnEnable(){
		parent = (AutoSorter) target;
		
	}
	
	
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI();
		
		if(GUILayout.Button("Sort Child")){
			parent.SortChild();
		}
		
		
	}
}
