﻿using UnityEngine;
using System.Collections;

public class GameEvent : MonoBehaviour {

	public delegate void ButtonHandler();
	public delegate void EnvironmentPoolHandler(int enviID);
	public delegate void PowerUpHandler(POWER_UP type);

	public delegate void GetCoinHandler(int pigID);
 
	#region Player Control 
	public static event ButtonHandler OnPlayerInput_JumpE;
	public static event ButtonHandler OnPlayerInput_MoveLeftE;
	public static event ButtonHandler OnPlayerInput_MoveRightE;
	public static event ButtonHandler OnPlayerInput_JumpBackE;
	public static event ButtonHandler OnPlayerInput_DashE;
	public static event ButtonHandler OnPlayerInput_PowerUpE; 


	public static void OnPlayerInput_Jump(){ if(OnPlayerInput_JumpE != null) { OnPlayerInput_JumpE(); }  }
	public static void OnPlayerInput_MoveLeft(){ if(OnPlayerInput_MoveLeftE != null) { OnPlayerInput_MoveLeftE(); }  }
	public static void OnPlayerInput_MoveRight(){ if(OnPlayerInput_MoveRightE != null) { OnPlayerInput_MoveRightE(); }  }
	public static void OnPlayerInput_JumpBack(){ if(OnPlayerInput_JumpBackE != null) { OnPlayerInput_JumpBackE(); }  }
	public static void OnPlayerInput_Dash(){ if(OnPlayerInput_DashE != null) { OnPlayerInput_DashE(); }  }
	public static void OnPlayerInput_PowerUp( ){ if(OnPlayerInput_PowerUpE != null) {OnPlayerInput_PowerUpE();} }
	#endregion


	#region GamePlay Events
	public static event ButtonHandler OnGameStartE;
	public static event EnvironmentPoolHandler OnEnvironmentTriggeredE;
	public static event PowerUpHandler OnPickUpPowerUpE;
	public static event ButtonHandler OnPigAttackedE;
	public static event ButtonHandler OnGameOverE;

	public static event GetCoinHandler OnGetCoinE;

	public static void OnGameStart(){ if(OnGameStartE != null) { OnGameStartE(); }  }
	public static void OnEnvironmentTriggered (int enviID) { if(OnEnvironmentTriggeredE!=null){ OnEnvironmentTriggeredE (enviID); } }
	public static void OnPickPowerUp(POWER_UP type){ if(OnPickUpPowerUpE != null) {OnPickUpPowerUpE(type);} }
	public static void OnPigAttacked () { if(OnPigAttackedE!=null){ OnPigAttackedE (); } }
	public static void OnGameOver(){ if(OnGameOverE != null) { OnGameOverE(); }  }

	public static void OnGetCoin (int pigID) { if(OnGetCoinE!=null){ OnGetCoinE (pigID); } }
	#endregion

}
