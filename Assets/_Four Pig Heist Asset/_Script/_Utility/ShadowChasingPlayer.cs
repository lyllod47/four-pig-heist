﻿using UnityEngine;
using System.Collections;

public class ShadowChasingPlayer : MonoBehaviour {

	public Transform targetPlayer;

	void FixedUpdate () {
	
		transform.position = new Vector3 (targetPlayer.position.x , 0 , targetPlayer.position.z);
	}
}
