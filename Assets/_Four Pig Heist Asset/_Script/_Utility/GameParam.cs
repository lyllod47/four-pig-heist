﻿using UnityEngine;
using System.Collections;

public class GameParam : MonoBehaviour {
	
	public static string TAG_PLAYER = "Player";
	
	public static string SCENE_GAME = "Scene_002_Game";
	public static string SCENE_TITLE = "Scene_001_Title_Screen";
	
	public static float[] LanesXPost = new float[]{ -3.9f, -1.3f, 1.3f, 3.9f };
	
}


public enum LANES {
	OUTER_LEFT ,
	LEFT ,
	RIGHT ,
	OUTER_RIGHT
}

public enum POWER_UP{
	NONE ,
	BOMB ,
	BANANA_SKIN ,
	SHIELD
}


public enum PIG_ACTIONS{
	NONE ,
	JUMP , 
	MOVE_LEFT ,
	MOVE_RIGHT ,
	JUMP_BACK,
	DASH ,
	USE_POWER_UPS
}

public enum OBJECT_IN_GAME{
	OBSTACLE ,
	POWERUP_BOX ,
	COINS
}



