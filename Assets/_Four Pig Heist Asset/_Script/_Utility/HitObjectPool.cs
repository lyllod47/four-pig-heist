﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HitObjectPool : MonoBehaviour {

	public GameObject poolPrefab;
	public List<GameObject> PoolList;

	void OnEnable(){
		GameEvent.OnGameOverE += DeactivateAll;
	}

	void OnDisable(){
		GameEvent.OnGameOverE -= DeactivateAll;
	}


	void DeactivateAll(){
		for (int i = 0; i < PoolList.Count; i++) 	{
			PoolList[i].gameObject.SetActive(false);  		
		}
	}
 

	public GameObject GetItem(){		
		for (int i = 0; i < PoolList.Count; i++) {
			if(!PoolList[i].gameObject.activeInHierarchy){
				PoolList[i].gameObject.SetActive(true);
				return PoolList[i];
			}
		}		
		return SpawnNewItem();		
	}

	GameObject SpawnNewItem(){ 
		GameObject newCarAI = Instantiate(poolPrefab , new Vector3 (100	,1 	,100) , Quaternion.identity) as GameObject;	
		newCarAI.transform.SetParent(transform);
		PoolList.Add( newCarAI );
		return newCarAI;
	}

	public int GetTotalActiveItem(){
		int result = 0;
		for (int i = 0; i < PoolList.Count; i++) {
			if(PoolList[i].gameObject.activeInHierarchy){
				result++;
			}
		}
		return result;
	}



}
