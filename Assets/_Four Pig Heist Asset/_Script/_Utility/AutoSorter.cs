﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class AutoSorter : MonoBehaviour {


	public Transform[] laneList;
	public float XOffset;



	public void SortChild(){

		laneList = laneList.OrderBy( go => go.name ).ToArray();

		for (int i = 0; i < laneList.Length; i++) {

			laneList[i].position = new Vector3 ( i*XOffset , 0 , 0 );
		}
	}


 
 

}
